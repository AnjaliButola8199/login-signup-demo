<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('',[AuthController::class,'login'])->name('login');
Route::post('check-user',[AuthController::class,'checkUser'])->name('check_exist');
Route::get('register',[AuthController::class, 'register'])->name('register');
Route::post('register-user',[AuthController::class, 'registerUser'])->name('user.register');
Route::post('login',[AuthController::class, 'passwordCheck'])->name('passwordCheck');

Route::group(['middleware' => 'auth:web'],function(){
    Route::get('dashboard',[AuthController::class, 'dashboard'])->name('dashboard');
    Route::get('logout',[AuthController::class, 'logout'])->name('logout');
});
Route::get('email-verify/{token}',[AuthController::class, 'verifyEmail'])->name('email.verify');
Route::post('otp-verify',[AuthController::class, 'otpVerify'])->name('otp.verify');
