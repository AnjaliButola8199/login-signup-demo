<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class EmailVerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    protected $link;
    protected $name;
    protected $otp;
    public function __construct($link,$name,$otp)
    {
        $this->link = $link;
        $this->name = $name;
        $this->otp  = $otp;
    }

    public function build()
    {
        return $this->view('email.emailVerificationEmail')
            ->subject("Account Verification Mail")
            ->from("anjali@yopmail.com")
            ->with([
                'link' => $this->link,
                'name' => $this->name,
                'otp'  => $this->otp,
            ]);
    }
}
