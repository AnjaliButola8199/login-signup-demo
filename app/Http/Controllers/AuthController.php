<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use Alert;
use App\Mail\EmailVerifyMail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function register(Request $request)
    {
        return view('auth.register',compact('request'));
    }

    public function registerUser(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email|unique:users,email',
            'dial_code' => 'required',
            'phone_number' => 'required|unique:users,phone_number',
            'profile' => 'required|image',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        $image              = $request->profile;
        $fileExtension      = $image->getClientOriginalExtension();
        $fileName           = time().rand(15,100).'.'.$fileExtension;
        $path               = "upload/profile/".date('FY');
        $image->move($path,$fileName);

        $user               = new User();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->phone_number = $request->phone_number;
        $user->dial_code    = $request->dial_code;
        $user->profile      = $path."/".$fileName;
        $user->otp          = rand(1000,9999);
        $user->token        = Str::random(64);
        $user->password     = Hash::make($request->password);
        $user->save();

        $link = route('email.verify',$user->token);
        $mail = new EmailVerifyMail($link,$user->name,$user->otp);
        Mail::to($request->email)->send($mail);

        Alert::success('','Account created succesfully.Please check mail');
        return redirect()->route('login');
    }
    public function verifyEmail($token){

        return view('verifyAccount',compact('token'));
    }

    public function otpVerify(Request $request){
        $user = User::where('token',$request->token)->first();
        if($user->email_verified_at != null){
            Alert::warning('','This email address is already connected with an account');
        }else if($user->otp == $request->otp){
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();

            Alert::success('','Email verified successfully.');
        }else{
            Alert::error('','Please try again, invalid OTP');
        }
        return redirect()->route('login');
    }
    public function checkUser(Request $request)
    {
        $request->validate([
            'username' => 'required'
        ]);

        $field = (is_numeric($request->username)) ?  'phone_number' : 'email' ;

        if($field == 'email'){
            $request->validate([
                'username' => 'email|max:150'
            ]);
        }else{
            $request->validate([
                'username' => 'numeric'
            ]);
        }

        $isExist = User::where($field,$request->username)->exists();
        if($isExist){
            return view('auth.password',compact('request','field'));
        }else{
            return redirect()->route('register');
        }
    }

    public function passwordCheck(Request $request)
    {
        $request->validate([
            'password' => 'required',
        ]);
        $credentials = [ $request->field => $request->username,'password' => $request->password ];
        // dd(Auth::check($credentials));
        if (Auth::attempt($credentials)) {
            // dd(Auth::user()->email_verified_at,Auth::User());
            if(Auth::user()->email_verified_at){
                $accessToken = Auth::user()->createToken('authToken')->accessToken;
                Alert::success('Welcome', 'Logged In Successfully.');
                return redirect()->route('dashboard');
            }else{
                // dd("else");
                Alert::warning('', 'Please verify your email address first.');
                return redirect()->route('login');
            }

        } else {
            // dd("else");
            Alert::warning('', 'Invalid username or password !!');
            return redirect()->route('login');
        }

    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function logout(Request $request)
    {
        // $request->user()->tokens()->delete();
        Auth::logout();
        return redirect()->route('login');
    }
}
