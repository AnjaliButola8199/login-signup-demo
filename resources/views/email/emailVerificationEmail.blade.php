<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Beta Booster | Verify Email</title>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <style>
        table {
            width: 600px;
            margin: 0 auto;
            clear: both;
            border-collapse: collapse;
            color: #202020;
        }
        a:hover, a:active, a:focus, a:focus-visible{
            text-decoration:none;
            color:#1C0385;
        }
        a{
            padding:10px 20px;
            border-radius:6px;
        }
    </style>
</head>

<body style="margin:0;padding:0;font-family: 'Rubik', sans-serif; font-size: 14px; color: #202020;" dir="ltr"
    bgcolor="#ffffff">
    <table border="0" cellspacing="0" cellpadding="0" align="center" width="600px">
        <tr>
            <td>
                <img class="header-img" src="{{url('assets/img/header_email.jpg')}}" width="600" height="84" />
            </td>
        </tr>
    </table>

    <table border="0" cellspacing="20" cellpadding="" align="center" width="600px"
        style="padding:10px; border:solid 1px #f1f1f1;">
        <tr>
            <td width="100%" style="height: 20px;"></td>
        </tr>

        <tr>
            <td width="100%" style="font-size:24px; color:#202020; font-weight: 500;padding: 0px 15px;">
                Hi {{$name}},
            </td>
        </tr>

        <tr>
            <td width="100%" style="height: 20px;"></td>
        </tr>

        <tr>
            <td width="100%" style="font-size:24px; color:#202020; font-weight: 500;padding: 0px 15px;">
                OTP for account verification is <b>{{ $otp }} </b>
            </td>
        </tr>

        <tr>
            <td width="100%" style="padding: 0px 15px;">
                <p style="margin:0px;line-height:25px;">To get your account verified, please click the button below</p>
            </td>
        </tr>

        <tr>
            <td width="100%" style="height: 10px;"></td>
        </tr>

        <tr>
            <td width="100%" style="padding: 0px 15px;">
                <a href="{{$link}}" style="background-color:#1C0385; color:#ffffff; border-radius:6px;line-height:30px;max-width: 100px; height:40px;vertical-align: middle; text-align:center;padding: 10px 20px;text-decoration: none;">Verify Account</a>
            </td>
        </tr>
        <tr>
            <td width="100%" style="height: 20px;"></td>
        </tr>
    </table>
</body>
</html>
